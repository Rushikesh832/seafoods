<?php
include 'buy/db.php';

    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            // Retrive order
            if(!empty($_GET["user_id"]))
            {
                $user_id=intval($_GET["user_id"]);
                $query="SELECT * from users where Id =  $user_id ";

                $result=mysqli_query($connection, $query);
                //echo $result->num_rows;
                if($result->num_rows == 0)
                {
                    $response=array(
                                        'status' => 402,
                                        'status_message' =>'User Id does Not Exist.'
                                    );
                    header('Content-Type: application/json');
                    echo json_encode($response);
                }else
                {
                    get_order($user_id);
                }
                       
                

            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'user_id is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
            }

            break;
        
        case 'POST':
            // Insert order


            if(empty($_POST["user_id"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'user id is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }else
            {
                $user_id=intval($_POST["user_id"]);
                $query="SELECT * from users where Id = $user_id ";

                $result=mysqli_query($connection, $query);
                //echo $result->num_rows;
                if($result->num_rows == 0)
                {
                    $response=array(
                                        'status' => 402,
                                        'status_message' =>'User Id does Not Exist.'
                                    );
                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
            }


             
            if(empty($_POST["city"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'city is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }
            if(empty($_POST["locality"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'Locality is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }
            if(empty($_POST["razor_pay_id"]))
            {
                $PaymentMethod = 'cash';
                $razor_pay_id=null;
            }
            else
            {
                $PaymentMethod = 'online';
                $razor_pay_id=$_POST["razor_pay_id"];
            }

            if(empty($_POST["address"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'address is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

			if(empty($_POST["deliveryslot"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'Delivery Slot is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }
						
			if(empty($_POST["deliverydate"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'Delivery Date is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }

            if(empty($_POST["DeliveryCharges"]))
            {
                 $response=array(
                'status' => 402,
                'status_message' =>'Delivery Charges is required.');


                header('Content-Type: application/json');
                echo json_encode($response);
                break;
            }
					
            else
            {
                placeOrderFromCart($_POST["user_id"],$_POST["address"],$_POST["city"],$_POST["locality"],$_POST["deliveryslot"],$_POST["deliverydate"],$_POST["DeliveryCharges"],$razor_pay_id,$PaymentMethod);
                break;
            }




            
        case 'DELETE':
            /*if(!empty($_GET["order_id"]))
            {
                $order_id=intval($_GET["order_id"]);
                delete_order($order_id);
            }
            else
            {

                if(empty($_GET["user_id"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'user_id is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                else
                {

                    delete_order_by_user_id($_GET
                        ["user_id"]);
                }
            }
            break;*/
        default:
            // Invalid Request Method
             $response=array(
                'status' => 402,
                'status_message' =>'Request Not Allowed.');


                header('Content-Type: application/json');
                echo json_encode($response);
            break;
    }



    function get_order($user_id)
    {
        global $connection;


        $query="SELECT * from Orders where Orders.UserId =  $user_id ";
        //echo $query;
        $response=array();
       // $resultfirst = $connection->query($queryfirst);
        $result=mysqli_query($connection, $query);
        if($result->num_rows)
        {


            while($row=$result->fetch_assoc())
            {
                $query1="SELECT * from OrderDetails where OrderDetails.OrderId =  $row[OrderId] ";
                $result1=mysqli_query($connection, $query1);
                if($result1->num_rows)
                {
                while($row1=$result1->fetch_assoc())
                    {
                        $row[]=$row1;
                    }
                }
                else
                {
                    $response1=array(
                                
                                'message' =>' orders does not contains products.'
                            );
                    $row[]=$response1;
                }

                $response[]=$row;
            }
        }
        else
        {
            $response=array(
                                'status' => 200,
                                'status_message' =>'no  orders yet.'
                            );
        }
        

        
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function placeOrderFromCart($user_id,$address,$city,$locality,$deliveryslot,$deliverydate,$DeliveryCharges,$razor_pay_id,$PaymentMethod)
    {
        global $connection;
        $query="SELECT * from users where Id =  $user_id ";
        $result=mysqli_query($connection, $query);
        if($result->num_rows == 0)
        {
            $response=array(
                                'status' => 402,
                                'status_message' =>'User Id does Not Exist.'
                            );
        }
        $query="SELECT * from Cart,product where Cart.UserId =  $user_id  and Cart.ProductId = product.id";
        //echo $query;die;
        $SubTotal=0;
        $Total=0;
        $TotalQuantity=0;
        $response=array();

        $result=mysqli_query($connection, $query);

       // $row1=$result->fetch_assoc();
        //print_r($row1);die;
        date_default_timezone_set("Asia/Kolkata");
        $current_date  = date("Y-m-d h:i:s");


        if($result->num_rows)
        {
            $query1 ="INSERT INTO `Orders`( `UserId` ,`DeliveryAddress`, City,Locality,`PaymentMethod`, `Status`,`RazorpayId`,`DeliveryDate`,`DeliverySlot`,`DeliveryCharges`,`OrderPlacedTime`) VALUES ($user_id,'$address','$city','$locality','$PaymentMethod','new','$razor_pay_id','$deliverydate','$deliveryslot','$DeliveryCharges','$current_date')";


            //echo $query1;die;
            $result1=mysqli_query($connection, $query1);




            if($result1)
            {
                $last_id = $connection->insert_id;
                while($row=$result->fetch_assoc())
                {
                   /* $sellPrice=$row['sellPrice'];
                    echo $sellPrice ;die;*/
                    $SubTotal=$row['sellPrice']*$row['Quantity'];
                    $Total=$Total + $SubTotal;
                    $TotalQuantity = $TotalQuantity + $row['Quantity'];

                    $query1="INSERT INTO `OrderDetails`(`ProductId`, `Quantity`, `SubTotal`, `OrderId`) VALUES ($row[id],$row[Quantity],$SubTotal,$last_id)";
                    $DaTa=mysqli_query($connection, $query1);

                    
                }



                $query2="UPDATE `Orders` SET `Total`=$Total+$DeliveryCharges,`Quantity`=$TotalQuantity WHERE OrderId=$last_id";
               // echo $query2;die;

                $result2=mysqli_query($connection, $query2);
                    if($result2)
                    {
                        $emptyCartQuery="DELETE FROM Cart WHERE UserId=$user_id";
                        $DATA=mysqli_query($connection, $emptyCartQuery);
                        if($DATA)
                        {
                           $response=array(
                                'status' => 200,
                                'status_message' =>'Order Placed Successfully.'
                            ); 
                        }
                        else
                        {
                            $response=array(
                                'status' => 500,
                                'status_message' =>'Internal Server Error At Deletion Of Cart.'
                            );
                         
                        }
                    }

            }
            else
            {
                $response=array(
                                'status' => 402,
                                'status_message' =>'Insertion Failed.'
                            );
            }



            
        }
        else
        {
            $response=array(
                                'status' => 402,
                                'status_message' =>'empty Cart.'
                            );
        }
        

        
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    



    function insert_order()
    {
        global $connection;
        $ProductId=$_POST["product_id"];
        $Quantity=$_POST["quantity"];
        $UserId=$_POST["user_id"];


        $query ="SELECT * from order where UserId = {$UserId} and ProductId = {$ProductId} Limit 1";
        $result=mysqli_query($connection, $query);
        if($result->num_rows)
        {
            $qty=0;
            while($row=$result->fetch_assoc())
                {
                   $qty = $Quantity + $row['Quantity'];
                   $id  = $row['orderId'];

                   if($qty == 0)
                   {
                        $Deletequery="DELETE FROM order  where orderId=".$id;

                        if(mysqli_query($connection, $Deletequery))
                        {
                            $response=array(
                                'status' => 200,
                                'status_message' =>'Product Deleted Successfully In order.'
                            );
                        }
                        else
                        {
                            $response=array(
                                'status' => 500,
                                'status_message' =>'Product Deletion Failed.'
                            );
                        }

                   }
                    elseif($qty < 0)
                   {
                             $response=array(
                                'status' => 200,
                                'status_message' =>"Quantity Cannot be Negative."
                            );
                   }
                   else
                   {



                       $Insertquery="UPDATE order SET Quantity={$qty} where orderId=".$id;

                        //echo $query;
                        if(mysqli_query($connection, $Insertquery))
                        {
                            $response=array(
                                'status' => 200,
                                'status_message' =>'order Updated Successfully In order.'
                            );
                        }
                        else
                        {
                            $response=array(
                                'status' => 500,
                                'status_message' =>'order Updation Failed.'
                            );
                        }
                    }

                }
        }
        else
        {

            $Insertquery="INSERT INTO order SET ProductId={$ProductId}, Quantity={$Quantity}, UserId={$UserId}";

            //echo $query;
            if(mysqli_query($connection, $Insertquery))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Product Added Successfully In order.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'order Addition Failed.'
                );
            }

        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
    }




   


    function delete_order($order_id = 0)
    {
         global $connection;


        $flag=0;
       
            $query1 ="SELECT * from order where orderId = {$order_id}  Limit 1";
            $result=mysqli_query($connection, $query1);
            if($result->num_rows)
            {
                $query="DELETE FROM order where orderId=$order_id";
            //echo $query;
            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'Insert valid id.');
                $flag=1;
            }
        

        //echo $query;die;
        if( $flag == 0)
        {


            if(mysqli_query($connection, $query))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Product Deleted Successfully.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'Product Deletion Failed.'
                );
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    function delete_order_by_user_id($user_id)
    {
        global $connection;


        $flag=0;
       
            $query1 ="SELECT * from order where UserId = {$user_id}  ";
            $result=mysqli_query($connection, $query1);
            if($result->num_rows)
            {
                $query="DELETE FROM order where UserId=$user_id";
            //echo $query;
            }
            else
            {
                $response=array(
                'status' => 402,
                'status_message' =>'id is not in order.');
                $flag=1;
            }
        

        //echo $query;die;
        if( $flag == 0)
        {


            if(mysqli_query($connection, $query))
            {
                $response=array(
                    'status' => 200,
                    'status_message' =>'Products Deleted Successfully.'
                );
            }
            else
            {
                $response=array(
                    'status' => 500,
                    'status_message' =>'Products Deletion Failed.'
                );
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    



    ?>