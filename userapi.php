<?php
include 'buy/db.php';

    $request_method=$_SERVER["REQUEST_METHOD"];
    switch($request_method)
    {
        case 'GET':
            // Retrive users
            if(empty($_GET["user_mail"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'user mail is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }

             elseif(empty($_GET["password"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'password is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                else
                {
                    login($_GET["user_mail"],$_GET["password"]);
                    break;
                }
        case 'POST':
            
            if(empty($_POST["user_mail"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'user mail is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }

             elseif(empty($_POST["password"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'password is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }

                    elseif(empty($_POST["Phone_number"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'Phone number is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                elseif(empty($_POST["address"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'address is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                elseif(empty($_POST["locality"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'locality is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                elseif(empty($_POST["city"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'city is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                elseif(empty($_POST["firstname"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'firstname is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                elseif(empty($_POST["lastname"]))
                {
                     $response=array(
                    'status' => 402,
                    'status_message' =>'lastname is required.');


                    header('Content-Type: application/json');
                    echo json_encode($response);
                    break;
                }
                else
                {
                    insert_user($_POST["user_mail"],$_POST["firstname"],$_POST["lastname"],$_POST["password"],$_POST["Phone_number"],$_POST["address"],$_POST["city"],$_POST["locality"]);
                    break;
                }



            break;
        case 'PUT':
            // Update user
            $user_id=intval($_GET["user_id"]);
            update_user($user_id);
            break;
        case 'DELETE':
            // Delete user
            $user_id=intval($_GET["user_id"]);
            delete_user($user_id);
            break;
        default:
            // Invalid Request Method
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }



    function login($mail,$password)
    {
        global $connection;
        $encryptedpass = sha1($password);
        $query="SELECT * FROM users where password='$encryptedpass' and email='$mail'";
        //echo $query;
        
        $result=mysqli_query($connection, $query);
        while($row=$result->fetch_assoc())
        {
           // print_r($row);die;
            $response[]=$row;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }



    function insert_user($mail,$firstname,$lastname,$password,$Phone_number,$address,$locality,$city)
    {
        global $connection;
        $encryptedpass = sha1($password);

        $query="INSERT INTO `users`( `email`, `firstname`, `phoneNumber`,`lastname`, `password`, `address`, `addressType`, `city`, `country`, `Locality`, `role`, `IsEmailVerify`, `VerificationCode`) VALUES ('$mail','$firstname','$Phone_number','$lastname','$encryptedpass','$address','home','$city','india','$locality','client',1,111111)";
        //echo $query;
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 200,
                'status_message' =>'user Added Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 500,
                'status_message' =>'user Addition Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }




    function update_user($user_id)
    {
        global $connection;
        parse_str(file_get_contents("php://input"),$post_vars);
        $user_name=$post_vars["user_name"];
        $price=$post_vars["price"];
        $quantity=$post_vars["quantity"];
        $seller=$post_vars["seller"];
        $query="UPDATE users SET user_name='{$user_name}', price={$price}, quantity={$quantity}, seller='{$seller}' WHERE id=".$user_id;
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 1,
                'status_message' =>'user Updated Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 0,
                'status_message' =>'user Updation Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }



    function delete_user($user_id)
    {
        global $connection;
        $query="DELETE FROM users WHERE id=".$user_id;
        if(mysqli_query($connection, $query))
        {
            $response=array(
                'status' => 1,
                'status_message' =>'user Deleted Successfully.'
            );
        }
        else
        {
            $response=array(
                'status' => 0,
                'status_message' =>'user Deletion Failed.'
            );
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }


    



    ?>