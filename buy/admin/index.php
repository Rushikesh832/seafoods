<?php session_start();

if ($_SESSION['role'] != 'admin') {
  header('Location:index.php');

}
if ($_SESSION['login'] != true ) {
    header('Location:signIn.php');
  }

include '../db.php';

$queryfirst = "SELECT * FROM Orders, users, OrderDetails,product where users.Id=Orders.UserId AND Orders.OrderId=OrderDetails.OrderId AND product.id=OrderDetails.ProductId";
                 
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  $newcount=0;
                  $pregresscount=0;
                  $Completedcount=0;
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                    if($rowfirst['Status'] == 'new' || $rowfirst['Status'] == 'New')
                    {
                      $newcount=$newcount+1;
                    }
                    elseif ($rowfirst['Status'] == 'progress' || $rowfirst['Status'] == 'Progress') {
                      
                      $pregresscount=$pregresscount+1;

                    }
                    elseif ($rowfirst['Status'] == 'complete' || $rowfirst['Status'] == 'paid' || $rowfirst['Status'] == 'Complete' || $rowfirst['Status'] == 'Paid')
                    {
                      $Completedcount=$Completedcount+1;
                    }
                    else
                    {
                      continue;
                    }

          }
        }

 require "../headers.php";
 ?>

 <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Admin Dashboard</h2>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="Product.php" class="img-prod"><img class="img-fluid" src="../images/phone.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="Product.php">Products</a></h3>
                </div>
            </div>
          </div>

           <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="showUsers.php" class="img-prod"><img class="img-fluid" src="../images/user.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="showUsers.php">Users</a></h3>
                </div>
            </div>
          </div>
           
           <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="category.php" class="img-prod"><img class="img-fluid" src="../images/gallary.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="category.php">Category</a></h3>
                </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="showOrders.php" class="img-prod"><img class="img-fluid" src="../images/stats.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="showOrders.php">Orders</a></h3>
                </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="banner.php" class="img-prod"><img class="img-fluid" src="../images/watch.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="banner.php">Banner</a></h3>
                </div>
            </div>
          </div>

           <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="locality.php" class="img-prod"><img class="img-fluid" src="../images/laptop.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="banner.php">Locality</a></h3>
                </div>
            </div>
          </div>



          <div class="col-md-6 col-lg-3 ftco-animate">
          <a href="showOrders.php"><div class="dashboard-window clearfix" style="background: #5cb85c; border-left: 5px solid #4f9f4f;height: 90px;">
                <div class="d-w-icon">
                    <h3><span class="glyphicon glyphicon-shopping-cart giant-white-icon"></span></h3>
                </div>
                <div class="d-w-text">
                    <h3><span class="d-w-num"><?php echo "New Orders="; ?></span><?php echo $newcount;?></h3>
                </div>
            </div></a>

            <a href="showOrders.php"><div class="dashboard-window clearfix" style="background: #62acec; border-left: 5px solid #5798d1;height: 90px;">
                <div class="d-w-icon">
                    <h2><span class="glyphicon glyphicon-shopping-cart giant-white-icon"></span></h2>
                </div>
                <div class="d-w-text">
                    <h3><span class="d-w-num"><?php echo "Pending Orders="; ?></span><?php echo $pregresscount;?></h3>
                </div>
            </div></a>

            <a href="showOrders.php"><div class="dashboard-window clearfix" style="background: #d9534f; border-left: 5px solid #b94643;height: 90px;">
                <div class="d-w-icon">
                    <h1><span class="glyphicon glyphicon-shopping-cart giant-white-icon"></span></h1>
                </div>
                <div class="d-w-text">
                    <h3><b><span class="d-w-num"><?php echo "Completed Orders="; ?></span><?php echo $Completedcount;?></b></h3>
                </div>
            </div></a>
          </div>


        </div>
      </div>
    </section>



       
 
 <?php require '../footer.php'; ?>
