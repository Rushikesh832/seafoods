<?php session_start();
  require '../headers.php'; 


 if (!isset($_SESSION['login'])) {
    header('Location:../../signIn.php');
  }

  if ($_SESSION['role'] != 'admin') {
  header('Location:../../index.php');

}
  
?>
  <style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>
  <hr size="2px">
    <!DOCTYPE html>
<html lang="en">

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="addProductData.php" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Add Product</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname"> Product Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Product Name" required="">
                  </div>
                </div>
                

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="category">Category</label></br>
                      <select name="category" style="width: 300px;" class="form-control">
                        <?php  
                        include '../db.php';
                          $queryfirst = "SELECT * FROM category";
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                          while($rowfirst = $resultfirst->fetch_assoc()) {
                              $name_best = $rowfirst['name'];
                              $id_best = $rowfirst['Id'];
                              $CustomCategory = $rowfirst['CustomCategory'];
           
                        ?>
                      
                      <option value="<?= $CustomCategory?>"><?= $name_best?></option>
                      <?php  }} ?>
                    </select>
                  </div>
                </div>


                
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="country">Description</label>
                    <input type="text"  name="description" class="form-control" placeholder="Description" required="">
                  </div>
                </div>
                
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Market Price</label>
                    <input type="text" class="form-control" name="mprice" placeholder="Market Price" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Selling Price</label>
                    <input type="text" class="form-control" name="sprice" placeholder="Selling Price" required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Net Wt</label>
                    <input type="text" class="form-control" name="netwt" placeholder="Net Wt" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Gross</label>
                    <input type="text" class="form-control" name="gross" placeholder="Gross" required="">
                  </div>
                </div>
                <div class="w-100"></div>

                 <div class="col-md-6">
                  <div class="form-group">
                    <label for="product-image">Thumbnail Image</label>
                    <input type="file" class="form-control"  name="thumbnail" placeholder="" required="">
                  </div>
                </div>
               
                <div class="w-100"></div>
                </br>

                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-success"  name ="addproduct" style="width: 120px; height: 38px">Add Product</button>
                 
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php  
        
    require '../footer.php'; ?>
    
  </body>
</html>