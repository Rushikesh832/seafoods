<?php session_start(); 

   if (!isset($_SESSION['login'])) 
   { 
     header('Location: signIn.php');
  }
  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
   require '../headers.php';


         
  
 ?>

 <!DOCTYPE html>
<html lang="en">
     


    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">All Orders</h2>
            <div style="text-align: right;">
            <a href="ExportOrders.php" class="btn btn-info" style="width: 130px; height: 40px;text-align: center;"><h4>Export Orders</h4></a>

              <!--<div class="dropup" style="text-align: left">
                  <button class="btn btn-success" type="button" data-toggle="dropdown">Orders Filter
                      <span class="caret"></span></button>
                  <ul class="dropdown-menu">

                      <li><a href="#total_orders">All Orders</a></li>
                      <li><a href="#new_orders">New</a></li>
                      <li><a href="#progress_orders">Progress</a></li>
                      <li><a href="#complete_orders">Complete</li>
                  </ul>
              </div>-->
                <input class="form-control" id="myInput" style="width: 200px;outline-style: inset" type="text" placeholder="Search...">
            </div>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    
                    <th>E-mail</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>

                   <th> Total</th>
                       <th> Quantity</th>
                      <th>Status</th>
                      <th> RazorpayId</th>
                      <th>OrderPlacedTime</th>
                     
                    
                    <th> Address</th>
                    <th>Address type</th>
                    
                    <th>City</th>

                    <th>Country</th>

                     <th>Delivery Slot</th>
                     <th>Delivery Date</th>
                      <th>Delivery Charges</th>
                     <th>Preview</th>
                    
                    
                  </tr>
                </thead>
                <?php

                 include '../db.php';
                  $queryfirst = "SELECT * FROM Orders, users where users.Id=Orders.UserId   ORDER by Orders.OrderPlacedTime DESC";
                 // echo $queryfirst;
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {



                        $OrderId = $rowfirst['OrderId'];
                        $email = $rowfirst['email'];

                        $firstname = $rowfirst['firstname'];
                        $lastname = $rowfirst['lastname'];
                      $phoneNumber = $rowfirst['phoneNumber'];
                        $address = $rowfirst['address'];
                        $addressType = $rowfirst['addressType'];
                        $city = $rowfirst['city'];
                        $country = $rowfirst['country'];
                        $Total = $rowfirst['Total'];
                        $Quantity = $rowfirst['Quantity'];
                        $RazorpayId = $rowfirst['RazorpayId'];
                        $Status = $rowfirst['Status'];
                        $DeliverySlot = $rowfirst['DeliverySlot'];
                        $DeliveryDate = $rowfirst['DeliveryDate'];
                        $DeliveryCharges = $rowfirst['DeliveryCharges'];
                        $OrderPlacedTime = $rowfirst['OrderPlacedTime'];

                        
                        
           
                        
           

            ?>
                <tbody id="myTable">

                  <tr class="text-center" >
                   
                  
                    <td class="email" style="width:50px">
                      <h3 ><?= $email ?></h3>
                    </td>
                    <td class="firstname">
                      <h3><?= $firstname ?></h3>
                    </td>
                    <td class="lastname">
                      <h3><?= $lastname ?></h3>
                    </td>

                    <td class="phoneNumber">
                      <h3><?= $phoneNumber ?></h3>
                    </td>


                    <td class="Total">
                      <h3><?= $Total ?></h3>
                       </td>
                       <td class="Quantity">
                      <h3><?= $Quantity ?></h3>
                      </td>
                      <td class="Status">
                          <?php  if ($Status == 'new' || $Status == 'New' ) {
                              ?>
                              <h3>
                                  <li class="nav-item dropdown">
                                      <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NEW</a>

                                      <div class="dropdown-menu" aria-labelledby="dropdown04">

                                          <a class="dropdown-item" href="ChangeStatus.php?status=progress&Orderid=<?= $OrderId ?>">Progress</a>
                                          <a class="dropdown-item" href="ChangeStatus.php?status=complete&Orderid=<?= $OrderId ?>">Complete</a>

                                      </div>
                                  </li>
                              </h3>
                              <?php

                          }  elseif($Status == 'progress' || $Status == 'Progress') {
                              ?>
                              <h3>
                                  <li class="nav-item dropdown">
                                      <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Progress</a>

                                      <div class="dropdown-menu" aria-labelledby="dropdown04">
                                          <a class="dropdown-item" href="ChangeStatus.php?status=complete&Orderid=
                            <?= $OrderId ?>">Complete</a>

                                      </div>
                                  </li>
                              </h3>
                              <?php

                          }
                          elseif($Status == 'complete' || $Status == 'Complete')  {
                              ?>
                              <h3>Complete
                              </h3>
                              <?php

                          }
                          else
                          { ?>
                              <h3>Complete
                              </h3>
                              <?

                          }?>

                      </td><td class="RazorpayId">
                      <h3><?= $RazorpayId ?></h3>
                      </td><td class="OrderPlacedTime">
                      <h3><?= $OrderPlacedTime ?></h3>
                      
                    </td><td class="address">
                      <h3><?= $address ?></h3>
                    </td><td class="addressType">
                      <h3><?= $addressType ?></h3>
                    </td>
                     <td class="city">
                      <h3><?= $city ?></h3>
                    </td>
                    
                    <td class="country">
                      <h3><?= $country ?></h3>
                    </td>

                   


                      <td class="DeliverySlot">
                      <h3><?= $DeliverySlot ?></h3>
                    </td>
										<td class="DeliveryDate">
                      <h3><?= $DeliveryDate ?></h3>
                    </td>

                    <td class="DeliveryDate">
                      <h3><?= $DeliveryCharges ?></h3>
                    </td>

                    <td class="Preview"><a href="OrderPreview.php?Id=<?= $OrderId; ?>" class="btn-Success">Preview</a></td>
                    
                    
                
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                <?php
                }} ?>
              </table>
            </div>
          </div>
           
        </div>
       
      </div>
    </section>

    



    

    
    <?php  require '../footer.php'; ?>
  
    
  </body>
</html>

<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>