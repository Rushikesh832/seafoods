<?php session_start(); 

   if (!isset($_SESSION['login'])) 
   { 
     header('Location: signIn.php');
  }
  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
   require '../headers.php';


         
  
 ?>

   <!DOCTYPE html>
<html lang="en">
   


    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Manage Product</h2>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">

          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <!-- <th>&nbsp;</th> -->
                     <th>ID</th>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    
                    <th>Selling Price</th>
                    <th>Net Wt</th>
                    <th>Gross</th>
                    
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <?php

                 include '../db.php';
                  $queryfirst = "SELECT product.id,product.name,product.netwt,product.gross,price,sellPrice,status,product.description,thumbnail,category.name as cname,category FROM product,category where category.CustomCategory=product.category";
                  
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $name_best = $rowfirst['name'];
                        $price_best = $rowfirst['price'];
                        $sellingPrice = $rowfirst['sellPrice'];
                        $netwt = $rowfirst['netwt'];
                        $gross = $rowfirst['gross'];
												
                        $status = $rowfirst['status'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
                        $categoryName = $rowfirst['cname'];
                        $category = $rowfirst['category'];
           
                        
           

            ?>
                <tbody>

                  <tr class="text-center">
                   
                    <td class="product-remove"><a href="productRemove.php/?id=<?=  $id_best ?>"><span class="ion-ios-close"></span></a></td>

                    <td class="image-prod"><div class="img" style="background-image:url(../../images/products/<?= $thumbnail_best ?>);"></div></td>
                    
                    <td class="ID">
                      <h3><?= $id_best?></h3>
                    </td>
                    
                    <td class="Name">
                      <h3><?= $name_best ?></h3>
                    </td>
                    <td class="Description" style="width: 350px">
                      <h3><?= $Description ?></h3>
                    </td>
                    <td class="Category">
                      <h3><?= $categoryName ?></h3>
                    </td>
                   
                    <td class="Selling Price">
                      <h3><?= $sellingPrice ?></h3>
                    </td>
										
                    <td class="Netwt">
                      <h3><?= $netwt ?></h3>
                    </td>
										<td class="Gross">
                      <h3><?= $gross ?></h3>
                    </td>


                    
                    <td class="Edit"><a href="ProductEdit.php?Id=<?= $id_best; ?>" class="btn-Success">Edit</a></td>
                    <td></td>
                    
                    
                  </tr>

                  
                </tbody>
                <?php
                }} ?>
              </table>
            </div>
          </div>
           
        </div>
       
      </div>
    </section>

    



    

    
    <?php  require '../footer.php'; ?>
  
    
  </body>
</html>