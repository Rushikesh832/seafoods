<?php session_start();
  require '../headers.php'; 

 if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}

$id =$_GET['Id'];



                 include '../db.php';
                  $queryfirst = "SELECT *,category.name as Cname,category.CustomCategory as catId  FROM product,category where product.id=$id and category.CustomCategory = product.category ";
                 // echo $queryfirst;die;
                $resultfirst = $connection->query($queryfirst);
                if (isset($resultfirst->num_rows)) {
                  while($rowfirst = $resultfirst->fetch_assoc()) {

                        $id_best = $rowfirst['id'];
                        $name_best = $rowfirst['name'];
                        $price_best = $rowfirst['price'];
                        $sellingPrice = $rowfirst['sellPrice'];
												$netwt = $rowfirst['netwt'];
												$gross = $rowfirst['gross'];
                        $status = $rowfirst['status'];
                        $Description = $rowfirst['description'];
                        $thumbnail_best = $rowfirst['thumbnail'];
                        $category = $rowfirst['category'];
                        $CategoryName = $rowfirst['Cname'];
                        $categoryId = $rowfirst['catId'];
           
                        
           

            }
          }
  
?>
  <style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

  <hr size="2px">
  <!DOCTYPE html>
<html lang="en">
  

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="updateProductData.php/?id=<?php echo $id_best?>" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Update Product</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname"> Product Name</label>
                    <input type="text" name="name" class="form-control" value="<?php  echo $name_best;?>" required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="category">Category</label></br>
                      <select name="category" style="width: 300px;height: 50px">
                        <option value="<?= $categoryId?>"><?= $CategoryName?></option>
                        <?php  
                        include '../db.php';
                          $queryfirst = "SELECT * FROM category ";
                        $resultfirst = $connection->query($queryfirst);
                        if (isset($resultfirst->num_rows)) {
                          while($rowfirst = $resultfirst->fetch_assoc()) {
                              $name_best = $rowfirst['name'];
                              $id_best = $rowfirst['CustomCategory'];
           
                        ?>
                      
                      <option value="<?= $id_best?>"><?= $name_best?></option>
                      <?php  }} ?>
                    </select>
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="country">Description</label>
                    <input type="text"  name="description" class="form-control" value="<?php  echo $Description;?>"  required="">
                  </div>
                </div>
                
                <div class="w-100"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="streetaddress">Market Price</label>
                    <input type="text" class="form-control" name="mprice" value="<?php  echo $price_best;?>"  required="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Selling Price</label>
                    <input type="text" class="form-control" name="sprice" value="<?php  echo $sellingPrice;?>" >
                  </div>
                </div>
                
								<div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Net wt</label>
                    <input type="text" class="form-control" name="netwt" value="<?php  echo $netwt;?>" >
                  </div>
                </div>
								
								<div class="col-md-6">
                  <div class="form-group">
                    <label for="towncity">Gross</label>
                    <input type="text" class="form-control" name="gross" value="<?php  echo $gross;?>" >
                  </div>
                </div>
								
                <div class="w-100"></div>

                 <div class="col-md-6">
                  <div class="form-group">
                    <label for="product-image">Thumbnail Image</label>
                    <input type="file" class="form-control"  name="thumbnail" alue="<?php  echo $thumbnail_best;?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="product-image">Product Status Off/On</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                      <label class="switch">
                          <?php
                          if ($status == 1)
                          { ?>

                          <input type="checkbox"  name ="status" checked>
                          <span class="slider round"></span>
                          <?php }
                          elseif ($status == 0)
                          { ?>

                          <input type="checkbox"  name ="status" unchecked>
                          <span class="slider round"></span>
                          <?php }
                          else{ ?>
                              <input type="checkbox"  name ="status" unchecked>
                          <span class="slider round"></span>
                         <?php }?>
                      </label>
                  </div>
                </div>
               
                <div class="w-100"></div>
                </br>

                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-success"  name ="addproduct" style="width: 120px; height: 38px">Update Product</button>
                 
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php  
        
    require '../footer.php'; ?>
    
  </body>
</html>