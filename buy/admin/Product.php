<?php session_start();

if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

 require "../headers.php";
 ?>

 <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Products</h2>
          </div>
        </div>      
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="addProduct.php" class="img-prod"><img class="img-fluid" src="../images/add.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="addProduct.php">Add Product</a></h3>
                </div>
            </div>
          </div>
           <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="showProduct.php" class="img-prod"><img class="img-fluid" src="../images/phone.png" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="showProduct.php">Manage Product</a></h3>
                </div>
            </div>
          </div>

          
        </div>
      </div>
    </section>

 
 <?php require '../footer.php'; ?>
