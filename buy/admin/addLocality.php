<?php session_start();
  require '../headers.php'; 

 if (!isset($_SESSION['login'])) {
    header('Location: ../../signIn.php');
  }

  if ($_SESSION['role'] != 'admin') {
  header('Location: ../../index.php');

}
  
?>
  <style type="text/css">
  hr {
  border-style: double;
  border-width: 2px;
}
</style>
<!DOCTYPE html>
<html lang="en">
  <hr size="2px">

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            <form action="addLocalityData.php" method="post"  enctype="multipart/form-data">
              <h3 class="mb-4 billing-heading">Add Locality</h3>
              <div class="row align-items-end">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname"> Locality Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter Locality Name" required="">
                  </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="country">Delivery Rate</label>
                    <input type="text"  name="DRate" class="form-control" placeholder="Enter Delivery Rate" required="">
                  </div>
                </div>
               
                <div class="w-100"></div>
                </br>

                <div class="col-md-12">
                  <div class="form-group mt-4">
                  <button type="submit" class="btn btn-success"  name ="addLocality" style="width: 120px; height: 38px">Add Locality</button>
                 
                  </div>
                </div>
              </div>
            </form><!-- END -->
          </div>
        </div>
      </div>
    </section> <!-- .section -->

    <?php  
        
    require '../footer.php'; ?>
    
  </body>
</html>